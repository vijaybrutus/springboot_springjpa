package com.example.SpringJPA.models;

import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SuperBuilder
public class Author extends BaseEntity {

    @Column(
            name="f_name",
            length = 35
    )
    private String firstName;
    private String lastName;
    @Column(
            nullable = false,
            unique = true
    )
    private String email;
    private int age;
    @Column(
            insertable = false
    )
    public LocalDateTime LastModified;
    @ManyToMany(mappedBy = "authors")
    private List<Courses> coursesList;
}
