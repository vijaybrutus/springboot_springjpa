package com.example.SpringJPA.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
public class Lecture extends BaseEntity{
    private String name;
    @ManyToOne
    @JoinColumn(
            name="section_id"
    )
    private Section section;

    @OneToOne
    @JoinColumn(name = "resources_id")
    private Resources resources;
}
