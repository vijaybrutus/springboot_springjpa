package com.example.SpringJPA.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class Section extends BaseEntity{
    private String name;
    private String sectionOrder;
    @ManyToOne
    @JoinColumn(
            name="course_id"
    )
    private Courses courses;
    @OneToMany(mappedBy = "section")
    private List<Lecture> lectureList;
}
