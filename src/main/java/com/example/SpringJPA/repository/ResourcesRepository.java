package com.example.SpringJPA.repository;

import com.example.SpringJPA.models.Resources;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourcesRepository extends JpaRepository<Resources,Integer> {
}
