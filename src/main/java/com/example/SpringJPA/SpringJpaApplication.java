package com.example.SpringJPA;

import com.example.SpringJPA.models.Author;
import com.example.SpringJPA.models.Resources;
import com.example.SpringJPA.models.Video;
import com.example.SpringJPA.repository.AuthorRepository;
import com.example.SpringJPA.repository.ResourcesRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringJpaApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringJpaApplication.class, args);
	}
	@Bean
	public CommandLineRunner commandLineRunner(ResourcesRepository resourcesRepository){
		return args->{
			var resources = Video.builder().name("Vijay").length(5).build();
			resourcesRepository.save(resources);
		};

	}
}

